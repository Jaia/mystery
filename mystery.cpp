#include <iostream>
#include <vector>
#include <string>
using namespace std;

void print(auto A)
{
   for (auto a : A) 
        cout <<a<<" ";

   cout<<endl;
}

void mystery1(auto& Data)
{
  cout<<endl<<"Mystery 1"<<endl<<"---------------------"<<endl;

  for ( int i = 0 ; i < Data.size( ) ; i++)
  {
    for ( int j = 0 ; j < i ; j++)
	if ( Data[ i ] < Data[ j ] )
	    swap( Data[ i ] , Data[ j ] );

    print(Data);
  }//end outer for (this brace is needed to include the print statement)

}

//... Other mysteries...

void mystery2 (auto& Data)
{
	cout<<endl<<"Mystery 2"<<endl<<"---------------------"<<endl;
	
	int i , j , min , tmp;
	for (int i = 0; i < Data.size()- 1; i++)
	{
		min = i;
		//Find the smallest number
	for (j = i + 1; j < Data.size() - 1; j++)
	{
		if (Data[j] < Data[min])
			min = j;
	}
		if (min != 1)
		{
			tmp = Data[min];
			Data[min] = Data[i];
			Data[i] = tmp;
			
			print(Data);
		}//end if statement
	}//end for statement
}//end void

void mystery3 (auto& Data)
{
	cout<<endl<<"Mystery 3"<<endl<<"-----------------------"<<endl;
	
	int nextNum, passNum, insertNum;
	//search the array
	for (nextNum = 1; nextNum < Data.size(); nextNum++)
	{
		insertNum = Data[nextNum];
		passNum = nextNum;
		//searching to place current element in location
		while (passNum > 0 && Data[passNum - 1] > insertNum)
		{
			Data[passNum] = Data[passNum - 1];
			passNum--; 
		}//end while
		Data[passNum] = insertNum;
		print (Data);
	}//end for
}//end void
		
	

int main()
{
    
  vector<int> Data = {36, 18, 22, 30, 29, 25, 12};

  vector<int> D1 = Data;
  vector<int> D2 = Data;
  vector<int> D3 = Data;

  mystery1(D1);
  mystery2(D2);
  mystery3(D3);

}
